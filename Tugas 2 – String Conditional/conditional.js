//If - else
var nama = "aris";
var peran = "WEREWOLF";
var pesan;
if (nama != "") {
    if (peran != "") {
        if (peran.toLowerCase() ==  'penyihir' ) {
            console.log("Selamat datang di Dunia Werewolf, "+nama);
            console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
        }
        if (peran.toLowerCase() ==  'guard' ) {
            console.log("Selamat datang di Dunia Werewolf, "+nama);
            console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        }
        if (peran.toLowerCase() ==  'werewolf' ) {
            console.log("Selamat datang di Dunia Werewolf, "+nama);
            console.log("Halo Werewolf "+nama+", kamu akan memakan mangsa  setiap malam!");
        } 
    } else {
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
    }
} else {
    console.log("Nama harus diisi!");
}

//Switch
var hari = 29;
var bulan = 5;
var tahun = 2000;

if (hari >= 1 && hari <= 31) {
   tanggal = hari;
}else{
    tanggal = "";
}

if (bulan >= 1 && bulan <= 12) {
    switch (bulan) {
        case 1: { nama_bulan = "Januari";  break; }
        case 2: { nama_bulan = "Febuari";  break; }
        case 3: { nama_bulan = "Maret";  break; }
        case 4: { nama_bulan = "April";  break; }
        case 5: { nama_bulan = "Mei";  break; }
        case 6: { nama_bulan = "Juni";  break; }
        case 7: { nama_bulan = "Juli";  break; }
        case 8: { nama_bulan = "Agustus";  break; }
        case 9: { nama_bulan = "September";  break; }
        case 10: { nama_bulan = "Oktober";  break; }
        case 11: { nama_bulan = "November";  break; }
        case 12: { nama_bulan = "Desember";  break; }
        
    }    
}

if (tahun >= 1900 && tahun <= 2200) {
    tahun_pilih = tahun;
}else{
    tahun_pilih = "";
}




console.log(tanggal+" "+nama_bulan+" "+tahun_pilih);