n1 = 2;
n2 = 20;
console.log("LOOPING PERTAMA");
while (n1 <= 20) {
    console.log(n1+" - I love coding" );
    n1=n1+2;
}

console.log("LOOPING KEDUA");
while (n2 >= 2) {
    console.log(n2+" - I will become a mobile developer" );
    n2=n2-2;
}

console.log("No. 2 For");
for (var n = 1; n <= 20; n++) {
    if (n%2 == 0) {
        h = "Berkualitas";
    }else if(n%3 == 0){
        h = "I Love Coding";
        
    }else{
        h = "Santai";
    }
    console.log(n+" - "+h);
}

console.log("No. 3");
for (var n = 1; n<=4; n++ ){
    var h ="";
    for(var m = 1; m<=8; m++){
        h += "#";
    }
 console.log(h);
}

console.log("No. 4");
var h ="";
for (var n = 1; n<=7; n++ ){
    for(var m = 1; m<=1; m++){
        h += "#";
    }
 console.log(h);
}

console.log("No. 5");
for (var n = 1; n<=8; n++ ){
    var h ="";
    for(var m = 1; m<=8; m++){
        if (n%2 == 0) {
            if (m%2 == 0) {
                h += " ";
            }else{
                h += "#";
            }    
        }else{
            if (m%2 == 1) {
                h += " ";
            }else{
                h += "#";
            }
        }
    }
 console.log(h);
}