console.log("No. 1");
function arrayToObject(arr) {
    var index;
    if (!arr) {
        console.log("kosong");
        
    }else{
        for (index = 0; index < arr.length; ++index) {
            // console.log(arr[index]);
            var data = {
                // console.log(arr[index]);  
                firstName: arr[index][0],
                lastName: arr[index][1],
                gender: arr[index][2],
                age: arr[index][3]      
            }
            console.log(data);
        }
    }
    
}
 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
arrayToObject([]) 

console.log("No. 2");
function shoppingTime(memberId, money) {
    switch(memberId){
        case '1820RzKrnWn08':  
            barang = [ 
            'Sepatu Stacattu',
            'Baju Zoro',
            'Baju H&N',
            'Sweater Uniklooh',
            'Casing Handphone' 
            ];
            var data = {
                noID : "1820RzKrnWn08",
                uang : "24750000",
                brg : barang,
                changeMoney : 0 
            }
            break
        case '82Ku8Ma742': 
            barang = [ 
            'Casing Handphone' 
            ];
            var data = {
                noID : "82Ku8Ma742",
                uang : "170000",
                brg : barang,
                changeMoney : 120000 
            }
            break
         case '234JdhweRxa53': 
            data = "Mohon maaf, uang tidak cukup";
            break
        
        default : { data = "Mohon maaf, toko X hanya berlaku untuk member saja"}
    }
    return data;
}

 
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("No. 3")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var index;
    var data = [];
    for (index = 0; index < arrPenumpang.length; ++index) {
        var isi = {
            // console.log(arr[index]);  
            penumpang: arrPenumpang[index][0],
            naikDari: arrPenumpang[index][1],
            tujuan: arrPenumpang[index][2],
            bayar: 2000      
        }
        
        data.push(isi);
    }
    return data;

}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]