console.log("No. 1");
function range(awal, akhir) {
    var number = [];
    if(!awal || !akhir ){
        number.push(-1);
    }else{
        if(awal <= akhir){
            for (awal; awal <= akhir; awal++) {
                number.push(awal);
            }
        }else if(awal >= akhir){
            for (awal; awal >= akhir; awal--) {
                number.push(awal);
            }
        }
    }
    console.log(number);
}
range(1,10);
range(1);
range(11,18);
range(54, 50);
range();


console.log("No. 2");
function rangeWithStep(awal, akhir, langkah) {
    var number = [];
    if(awal <= akhir){
        for (awal; awal <= akhir; awal++) {
            if(awal % langkah == 0){
                number.push(awal-1);
            }                
        }   
    }else if(awal >= akhir){
        for (awal; awal >= akhir; awal--) {
            if(awal % langkah == 0){
                number.push(awal+1);
            }                
        }
    }
    console.log(number);
}
rangeWithStep(1, 10, 2);
rangeWithStep(11,23,3);
rangeWithStep(5, 2, 1);
rangeWithStep(29, 2, 4);

console.log("No. 3");
function sum(awal, akhir, langkah=1) {
   	var number = [];
  	if(awal <= akhir){
        for (awal; awal <= akhir; awal++) {
            if(awal % langkah == 0){
                number.push(awal);
            }                
        }   
    }else if(awal >= akhir){
        for (awal; awal >= akhir; awal--) {
            if(awal % langkah == 0){
                number.push(awal);
            }                
        }
    }
    var sum = number.reduce(function(a, b){
        return a + b;
    }, 0);
    
    console.log(sum);
}
sum(1,10);
sum(5,50,2);
// rangeWithStep(5,50,2);
sum(15,10);
sum(20, 10, 2);
sum(1);
sum();

console.log("No. 4")
function dataHandling() {
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]  
    
    console.log("Nomor ID: "+input[0][0]);
    console.log("Nama Lengkap: "+input[0][1]);
    console.log("TTL: "+input[0][3]);
    console.log("Hobi: "+input[0][2]);
    
    console.log("Nomor ID: "+input[1][0]);
    console.log("Nama Lengkap: "+input[1][1]);
    console.log("TTL: "+input[1][3]);
    console.log("Hobi: "+input[1][2]);

    console.log("Nomor ID: "+input[2][0]);
    console.log("Nama Lengkap: "+input[2][1]);
    console.log("TTL: "+input[2][3]);
    console.log("Hobi: "+input[2][2]);

    console.log("Nomor ID: "+input[3][0]);
    console.log("Nama Lengkap: "+input[3][1]);
    console.log("TTL: "+input[3][3]);
    console.log("Hobi: "+input[3][2]);
}

dataHandling();

console.log("No. 5	");
function balikKata(tulisan) {
	max = tulisan.length;
	var h;
    for (awl = 0 ; awl <= tulisan.length; awl++) {
    	n = max - awl;
        h =  h+tulisan[n];
    }
    return h ;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log("No. 6 ");
function dataHandling2(tulisan) {
	var nama = tulisan.slice(1,2);
	var nama_baru = nama+"Elsharawy";
	var lokasi = tulisan.slice(2,3);
	var lokasi_baru = "Provinsi "+lokasi;
	var waktu = ""+tulisan.slice(3,4);
	var bulan = waktu.split('/');
	switch(bulan[1]){
		 case '05': { output2 = "Mei";  break; }
	}	

	var output1 = tulisan.splice(0,1);
	output1.push(nama_baru, lokasi_baru, waktu, 'Pria','SMA Internasional Metro' );

	var output3 = [];
	output3.push(bulan[2], bulan[0], bulan[1]);
	
	var output4 = bulan.join("-");

	var output5 = nama;

	console.log(output1);	
	console.log(output2);
	console.log(output3);
	console.log(output4);
	console.log(output5);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);